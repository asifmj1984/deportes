-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-05-2020 a las 23:08:20
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcaracteristica`
--

CREATE TABLE `tblcaracteristica` (
  `idCaracteristica` int(11) NOT NULL,
  `strNombre_1` varchar(150) NOT NULL,
  `refPadre` int(11) NOT NULL,
  `intEstado` int(11) NOT NULL,
  `intOrden` int(11) NOT NULL,
  `strNombre_2` varchar(150) DEFAULT NULL,
  `strNombre_3` varchar(150) DEFAULT NULL,
  `strNombre_4` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcaracteristica`
--

INSERT INTO `tblcaracteristica` (`idCaracteristica`, `strNombre_1`, `refPadre`, `intEstado`, `intOrden`, `strNombre_2`, `strNombre_3`, `strNombre_4`) VALUES
(1, 'Accesorios', 0, 1, 1, 'Accesorios', 'Accesorios', 'Accesorios'),
(2, 'Fabricacion', 0, 1, 2, 'Fabricacion', 'Fabricacion', 'Fabricacion'),
(3, 'Tipo Ropa', 0, 1, 3, 'Tipo Ropa', 'Tipo Ropa', 'Tipo Ropa'),
(4, 'Ropa de temporada', 0, 1, 4, 'Season clothes', 'Vetements de saison', 'Roupas de temporada'),
(5, 'Frio', 4, 1, 1, 'Frio', 'Frio', 'Frio'),
(6, 'Calor', 4, 1, 2, 'Calor', 'Calor', 'Calor'),
(7, 'Propia', 2, 1, 1, 'Propia', 'Propia', 'Propia'),
(8, 'Extranjera', 2, 1, 2, 'Extranjera', 'Extranjera', 'Extranjera'),
(12, 'Sin Accesorios', 1, 1, 1, 'Sin Accesorios', 'Sin Accesorios', 'Sin Accesorios'),
(13, 'Con Accesorios', 1, 1, 2, 'Con Accesorios', 'Con Accesorios', 'Con Accesorios'),
(15, 'Lana', 3, 1, 0, 'Lana', 'Lana', 'Lana'),
(16, 'Algodon', 3, 1, 25, 'Algodon', 'Algodon', 'Algodon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcarrito`
--

CREATE TABLE `tblcarrito` (
  `idContador` int(11) NOT NULL,
  `refUsuario` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL,
  `intCantidad` int(11) NOT NULL,
  `intTransaccionEfectuada` int(11) NOT NULL DEFAULT '0',
  `dblTotalProducto` double(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcarrito`
--

INSERT INTO `tblcarrito` (`idContador`, `refUsuario`, `refProducto`, `intCantidad`, `intTransaccionEfectuada`, `dblTotalProducto`) VALUES
(36, 14, 6, 3, 3, NULL),
(37, 5, 1, 1, 4, NULL),
(39, 5, 1, 2, 5, NULL),
(40, 5, 1, 3, 5, NULL),
(41, 5, 5, 1, 6, NULL),
(42, 5, 4, 1, 7, NULL),
(44, 1, 1, 1, 0, NULL),
(45, 1, 6, 1, 0, NULL),
(46, 41, 1, 10, 0, NULL),
(47, 41, 6, 1, 0, NULL),
(48, 42, 1, 1, 0, NULL),
(49, 43, 8, 1, 0, NULL),
(50, 44, 1, 1, 8, 13100.00),
(51, 45, 1, 1, 9, 13100.00),
(52, 45, 6, 1, 10, 60500.00),
(61, 5, 2, 1, 11, 88.66),
(62, 5, 3, 3, 11, 171.36),
(63, 5, 6, 3, 12, 185.09),
(64, 5, 4, 3, 12, 849.42),
(65, 5, 15, 3, 12, 36.30),
(67, 45, 1, 3, 13, 4518.15),
(68, 45, 6, 5, 13, 293.07),
(69, 45, 4, 8, 13, 2265.12),
(70, 45, 3, 2, 14, 114.24),
(73, 45, 4, 1, 15, 283.14),
(74, 49, 3, 1, 0, NULL),
(75, 49, 5, 1, 0, NULL),
(76, 45, 7, 28, 15, 813.12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcarritodetalle`
--

CREATE TABLE `tblcarritodetalle` (
  `idContadorDetalle` int(11) NOT NULL,
  `refCarrito` int(11) NOT NULL,
  `refOpcion` int(11) NOT NULL,
  `refOpcionSeleccionada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcarritodetalle`
--

INSERT INTO `tblcarritodetalle` (`idContadorDetalle`, `refCarrito`, `refOpcion`, `refOpcionSeleccionada`) VALUES
(12, 17, 2, 5),
(13, 17, 8, 9),
(14, 18, 2, 6),
(15, 18, 8, 9),
(18, 20, 2, 5),
(19, 20, 8, 10),
(20, 22, 2, 5),
(21, 22, 8, 9),
(22, 23, 2, 5),
(23, 23, 8, 9),
(24, 25, 2, 6),
(25, 25, 8, 9),
(26, 26, 2, 5),
(27, 26, 8, 9),
(28, 27, 2, 6),
(29, 27, 8, 10),
(30, 31, 2, 5),
(31, 31, 8, 9),
(32, 32, 2, 5),
(33, 32, 8, 9),
(34, 33, 2, 5),
(35, 33, 8, 9),
(36, 35, 2, 5),
(37, 35, 8, 9),
(38, 37, 2, 5),
(39, 37, 8, 9),
(40, 38, 2, 5),
(41, 38, 8, 9),
(42, 39, 2, 5),
(43, 39, 8, 9),
(44, 40, 2, 5),
(45, 40, 8, 10),
(46, 43, 2, 5),
(47, 43, 8, 9),
(48, 44, 2, 5),
(49, 44, 8, 9),
(50, 46, 2, 5),
(51, 46, 8, 9),
(52, 48, 2, 5),
(53, 48, 8, 9),
(54, 50, 2, 5),
(55, 50, 8, 9),
(56, 51, 2, 5),
(57, 51, 8, 9),
(58, 53, 2, 5),
(59, 53, 8, 9),
(60, 53, 2, 5),
(61, 53, 8, 9),
(62, 57, 2, 5),
(63, 57, 8, 9),
(64, 58, 2, 5),
(65, 58, 8, 9),
(66, 61, 1, 3),
(67, 66, 1, 3),
(68, 67, 2, 6),
(69, 67, 8, 9),
(70, 71, 2, 6),
(71, 71, 8, 9),
(72, 75, 1, 3),
(73, 75, 2, 6),
(74, 75, 8, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcategoria`
--

CREATE TABLE `tblcategoria` (
  `idCategoria` int(11) NOT NULL,
  `strNombre_1` varchar(50) NOT NULL,
  `strSEO_1` varchar(50) NOT NULL,
  `intEstado` int(11) NOT NULL,
  `refPadre` int(11) NOT NULL,
  `intOrden` int(11) NOT NULL,
  `intPrincipal` int(11) NOT NULL DEFAULT '0',
  `strNombre_2` varchar(50) DEFAULT NULL,
  `strSEO_2` varchar(50) DEFAULT NULL,
  `strNombre_3` varchar(50) DEFAULT NULL,
  `strSEO_3` varchar(50) DEFAULT NULL,
  `strNombre_4` varchar(50) DEFAULT NULL,
  `strSEO_4` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcategoria`
--

INSERT INTO `tblcategoria` (`idCategoria`, `strNombre_1`, `strSEO_1`, `intEstado`, `refPadre`, `intOrden`, `intPrincipal`, `strNombre_2`, `strSEO_2`, `strNombre_3`, `strSEO_3`, `strNombre_4`, `strSEO_4`) VALUES
(1, 'Hombres', 'hombres', 1, 0, 7, 0, 'Man', 'man', 'Homme', 'homme', 'Cara', 'cara'),
(3, 'Outlet', 'outlet', 1, 0, 8, 0, 'Outlet', 'outlet', 'Outlet', 'outlet', 'Outlet', 'outlet'),
(4, 'Mujer', 'mujer', 1, 0, 2, 1, 'Woman', 'woman', 'Femme', 'femme', 'Mulher', 'mulher'),
(5, 'Último modelo', 'ultimo-modelo', 1, 0, 10, 1, 'Last model', 'last-model', 'Dernier modèle', 'dernier-mod-le', 'Modelo mais recente', 'modelo-mais-recente'),
(6, 'Camisetas', 'camisetas', 1, 1, 3, 0, 'T-Shirts', 't-shirts', 'T-Shirts', 't-shirts', 'Camisetas', 'camisetas'),
(11, 'Pantalon', 'pantalon', 1, 1, 4, 0, 'Pants', 'pants', 'Pantalon', 'pantalon', 'Calças', 'calcas'),
(12, 'Pantalon corto', 'pantalon-cort', 1, 11, 5, 1, 'Short Pants', 'short-pants', 'Pantalon court', 'pantalon-court', 'Calção', 'calcao'),
(13, 'Zapatos', 'zapatos', 1, 0, 9, 0, 'Shoes', 'shoes', 'Chaussures', 'chaussures', 'Sapatos', 'sapatos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcomentario`
--

CREATE TABLE `tblcomentario` (
  `idComentario` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL,
  `intEstado` int(11) NOT NULL DEFAULT '0',
  `strNombreComentador` varchar(50) NOT NULL,
  `strFecha` datetime NOT NULL,
  `refUsuario` int(11) NOT NULL DEFAULT '0',
  `txtComentario` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcomentario`
--

INSERT INTO `tblcomentario` (`idComentario`, `refProducto`, `intEstado`, `strNombreComentador`, `strFecha`, `refUsuario`, `txtComentario`) VALUES
(10, 6, 1, 'Registrado', '2020-05-22 20:43:23', 5, 'Es una mierda de coce'),
(11, 6, 1, 'Registrado', '2020-05-22 20:43:40', 5, 'S>gdgsdg'),
(12, 5, 1, 'Registrado', '2020-05-22 20:47:13', 5, ' bnm'),
(13, 5, 1, 'Registrado', '2020-05-22 20:47:28', 5, 'vbccvcvb'),
(14, 6, 1, 'Registrado', '2020-05-22 20:47:39', 5, 'cvbcvb'),
(15, 6, 1, 'Registrado', '2020-05-22 20:56:16', 5, 'sdaSASd'),
(16, 21, 1, 'Asgiiyt', '2020-05-25 19:34:08', 0, 'cfguuj vhjcfuux  '),
(17, 13, 1, 'e', '2020-05-25 23:48:31', 0, 'WEWEWE'),
(18, 4, 1, 'Registrado', '2020-05-26 17:15:14', 45, 'Muy pero que muy buen pantalon'),
(19, 1, 1, 'Registrado', '2020-05-26 18:26:30', 45, 'Me parece una muy buena bermuda.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcomparar`
--

CREATE TABLE `tblcomparar` (
  `idComparar` int(11) NOT NULL,
  `refUsuario` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcomparar`
--

INSERT INTO `tblcomparar` (`idComparar`, `refUsuario`, `refProducto`) VALUES
(8, 5, 8),
(9, 5, 2),
(10, 5, 4),
(11, 45, 3),
(12, 45, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcompra`
--

CREATE TABLE `tblcompra` (
  `idCompra` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `fchCompra` datetime NOT NULL,
  `intTipoPago` int(11) NOT NULL,
  `dblTotalIVA` double(8,2) NOT NULL,
  `dblTotalsinIVA` double(8,2) NOT NULL,
  `intFacturacion` int(11) NOT NULL,
  `intEnvio` int(11) NOT NULL,
  `intEstado` int(11) NOT NULL,
  `intZona` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `strDireccion` varchar(50) NOT NULL,
  `strProvincia` varchar(50) NOT NULL,
  `strPais` varchar(50) NOT NULL,
  `strCP` varchar(10) NOT NULL,
  `strEmail` varchar(100) NOT NULL,
  `strTelefono` varchar(50) NOT NULL,
  `refMoneda` int(11) DEFAULT NULL,
  `txtEmail` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblcompra`
--

INSERT INTO `tblcompra` (`idCompra`, `idUsuario`, `fchCompra`, `intTipoPago`, `dblTotalIVA`, `dblTotalsinIVA`, `intFacturacion`, `intEnvio`, `intEstado`, `intZona`, `strNombre`, `strDireccion`, `strProvincia`, `strPais`, `strCP`, `strEmail`, `strTelefono`, `refMoneda`, `txtEmail`) VALUES
(10, 45, '2020-05-22 19:39:19', 1, 60500.00, 50000.00, 0, 0, 0, 0, 'Asif Malik', 'C/ RAFAEL PASCUAL, 8, 1º, IBI', 'ALICANTE', 'ESPAÑA', '03440', 'asif@asif.com', '123554455555555', 1, 'Nombre Completo: Asif Malik<br>\r\nDirección: C/ RAFAEL PASCUAL, 8, 1º, IBI<br>\r\nProvincia: ALICANTE<br>\r\nPais: ESPAÑA<br>\r\nCódigo Postal: 03440<br>\r\nE-mail: asif@asif.com<br>\r\nTeléfono: 123554455555555<br><br><br><br>Pago transferencia<br><br>\r\nTransferencia Bancaria o Ingreso en Cuenta:<br><br>\r\n\r\nIndicar en el concepto el curso o cursos correspondientes.<br><br>\r\n\r\nBankia en España:<br>\r\nTitular: ASIF MALIK JAVED<br>\r\nNúmero de cuenta: XXXX-XXXX-XX-XXXXXXXXXX<br>\r\nIBAN: XXXX XXXX XXXX XXXX XXXX XXXX<br>\r\nBIC/SWIFT: CAHMESMMXXX<br>\r\nCiudad: Ibi<br>\r\nPaís: España<br><br>\r\n\r\nEn caso de realizar transferencia bancaria desde fuera de España, se deberán abonar 11 euros extra en concepto de gastos de transferencia.  <table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n  <tbody><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/baratos/cabriolo.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/coche4.jpg\" width=\"50px\" ></a></td><td>Cabriolo<br></td><td>1</td><td>60500,00 </td></tr></tbody></table><table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n     <tbody>\r\n       <tr>\r\n         <td width=\"61%\">&nbsp;</td>\r\n         <td width=\"23%\">SubTotal</td>\r\n         <td width=\"16%\">50000,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Impuesto</td>\r\n         <td>10500,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Transporte</td>\r\n         <td>0,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Total</td>\r\n         <td>60500,00 </td>\r\n       </tr>\r\n     </tbody>\r\n   </table>'),
(11, 5, '2020-05-23 23:11:16', 1, 260.02, 250.99, 0, 0, 1, 0, 'Pepe', 'Lopez', 'Valencia', 'España', '2343', 'pepe@pdpd.com', '879879', 1, 'Nombre Completo: Pepe<br>\r\nDirección: Lopez<br>\r\nProvincia: Valencia<br>\r\nPais: España<br>\r\nCódigo Postal: 2343<br>\r\nE-mail: pepe@pdpd.com<br>\r\nTeléfono: 879879<br><br><br><br>Pago transferencia<br><br>\r\nTransferencia Bancaria o Ingreso en Cuenta:<br><br>\r\n\r\nIndicar en el concepto el curso o cursos correspondientes.<br><br>\r\n\r\nBankia en España:<br>\r\nTitular: ASIF MALIK JAVED<br>\r\nNúmero de cuenta: XXXX-XXXX-XX-XXXXXXXXXX<br>\r\nIBAN: XXXX XXXX XXXX XXXX XXXX XXXX<br>\r\nBIC/SWIFT: CAHMESMMXXX<br>\r\nCiudad: Ibi<br>\r\nPaís: España<br><br>\r\n\r\nEn caso de realizar transferencia bancaria desde fuera de España, se deberán abonar 11 euros extra en concepto de gastos de transferencia.  <table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n  <tbody><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/bermudas-tipo-chino.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/bermudas-estampadas-all-over-marron-hombre-xl478_2_frf1.jpg\" width=\"50px\" ></a></td><td>Bermudas tipo chino<br>Color: Rojo <br></td><td>1</td><td>88,66 </td></tr><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/camisa-slim-de-flores.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf1.jpg\" width=\"50px\" ></a></td><td>Camisa slim de flores<br></td><td>3</td><td>171,36 </td></tr></tbody></table><table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n     <tbody>\r\n       <tr>\r\n         <td width=\"61%\">&nbsp;</td>\r\n         <td width=\"23%\">SubTotal</td>\r\n         <td width=\"16%\">250,99 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Impuesto</td>\r\n         <td>9,03 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Transporte</td>\r\n         <td>0,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Total</td>\r\n         <td>260,02 </td>\r\n       </tr>\r\n     </tbody>\r\n   </table>'),
(12, 5, '2020-05-25 00:54:41', 6, 1070.81, 884.97, 0, 0, 0, 0, 'Pepe', 'Lopez', 'Valencia', 'España', '2343', 'pepe@pdpd.com', '879879', 1, 'Nombre Completo: Pepe<br>\r\nDirección: Lopez<br>\r\nProvincia: Valencia<br>\r\nPais: España<br>\r\nCódigo Postal: 2343<br>\r\nE-mail: pepe@pdpd.com<br>\r\nTeléfono: 879879<br><br><br><br>Pago por Western Union: <table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n  <tbody><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/camiseta-bordada.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf1.jpg\" width=\"50px\" ></a></td><td>Camiseta bordada<br></td><td>3</td><td>185,09 </td></tr><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/pantalon-chino-de-sarga.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/vaquero-skinny-destroy-azul-tallas-grandes-mujer-wz450_4_frf1.jpg\" width=\"50px\" ></a></td><td>Pantalón chino de sarga<br></td><td>3</td><td>849,42 </td></tr><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/mujer/vestido-cruzado.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/vestido-cruzado-rosa-mujer-talla-34-a-48-xj633_5_frf1.jpg\" width=\"50px\" ></a></td><td>Vestido cruzado<br></td><td>3</td><td>36,30 </td></tr></tbody></table><table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n     <tbody>\r\n       <tr>\r\n         <td width=\"61%\">&nbsp;</td>\r\n         <td width=\"23%\">SubTotal</td>\r\n         <td width=\"16%\">884,97 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Impuesto</td>\r\n         <td>185,84 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Transporte</td>\r\n         <td>0,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Total</td>\r\n         <td>1070,81 </td>\r\n       </tr>\r\n     </tbody>\r\n   </table>'),
(13, 45, '2020-05-25 19:40:06', 5, 7076.34, 6629.20, 0, 0, 0, 0, 'Asif Malik', 'C/ RAFAEL PASCUAL, 8, 1º, IBI', 'ALICANTE', 'ESPAÑA', '03440', 'asif@asif.com', '123554455555555', 1, 'Nombre Completo: Asif Malik<br>\r\nDirección: C/ RAFAEL PASCUAL, 8, 1º, IBI<br>\r\nProvincia: ALICANTE<br>\r\nPais: ESPAÑA<br>\r\nCódigo Postal: 03440<br>\r\nE-mail: asif@asif.com<br>\r\nTeléfono: 123554455555555<br><br><br><br>Pago por Western Union: <table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n  <tbody><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/mujer/bermudas-vaqueras-tipo-jogging.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/camisa-comoda-de-punto-azul-tallas-grandes-hombre-vs913_35_frf1.jpg\" width=\"50px\" ></a></td><td>Bermudas vaqueras tipo jogging<br>Talla: L<br>Color Super Opel: Gris Opel<br></td><td>3</td><td>4518,15 </td></tr><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/camiseta-bordada.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf1.jpg\" width=\"50px\" ></a></td><td>Camiseta bordada<br></td><td>5</td><td>293,07 </td></tr><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/pantalon-chino-de-sarga.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/vaquero-skinny-destroy-azul-tallas-grandes-mujer-wz450_4_frf1.jpg\" width=\"50px\" ></a></td><td>Pantalón chino de sarga<br></td><td>8</td><td>2265,12 </td></tr></tbody></table><table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n     <tbody>\r\n       <tr>\r\n         <td width=\"61%\">&nbsp;</td>\r\n         <td width=\"23%\">SubTotal</td>\r\n         <td width=\"16%\">6629,20 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Impuesto</td>\r\n         <td>447,13 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Transporte</td>\r\n         <td>0,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Total</td>\r\n         <td>7076,34 </td>\r\n       </tr>\r\n     </tbody>\r\n   </table>'),
(14, 45, '2020-05-25 22:46:44', 1, 114.24, 112.00, 0, 0, 1, 0, 'Asif Malik', 'C/ RAFAEL PASCUAL, 8, 1º, IBI', 'ALICANTE', 'ESPAÑA', '03440', 'asif@asif.com', '123554455555555', 1, 'Nombre Completo: Asif Malik<br>\r\nDirección: C/ RAFAEL PASCUAL, 8, 1º, IBI<br>\r\nProvincia: ALICANTE<br>\r\nPais: ESPAÑA<br>\r\nCódigo Postal: 03440<br>\r\nE-mail: asif@asif.com<br>\r\nTeléfono: 123554455555555<br><br><br><br>Pago transferencia<br><br>\r\nTransferencia Bancaria o Ingreso en Cuenta:<br><br>\r\n\r\nIndicar en el concepto el curso o cursos correspondientes.<br><br>\r\n\r\nBankia en España:<br>\r\nTitular: ASIF MALIK JAVED<br>\r\nNúmero de cuenta: XXXX-XXXX-XX-XXXXXXXXXX<br>\r\nIBAN: XXXX XXXX XXXX XXXX XXXX XXXX<br>\r\nBIC/SWIFT: CAHMESMMXXX<br>\r\nCiudad: Ibi<br>\r\nPaís: España<br><br>\r\n\r\nEn caso de realizar transferencia bancaria desde fuera de España, se deberán abonar 11 euros extra en concepto de gastos de transferencia.  <table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n  <tbody><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/camisa-slim-de-flores.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf1.jpg\" width=\"50px\" ></a></td><td>Camisa slim de flores<br></td><td>2</td><td>114,24 </td></tr></tbody></table><table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n     <tbody>\r\n       <tr>\r\n         <td width=\"61%\">&nbsp;</td>\r\n         <td width=\"23%\">SubTotal</td>\r\n         <td width=\"16%\">112,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Impuesto</td>\r\n         <td>2,24 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Transporte</td>\r\n         <td>0,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Total</td>\r\n         <td>114,24 </td>\r\n       </tr>\r\n     </tbody>\r\n   </table>'),
(15, 45, '2020-05-28 22:21:42', 6, 1096.26, 906.00, 0, 0, 1, 0, 'Asif Malik', 'C/ RAFAEL PASCUAL, 8, 1º, IBI', 'ALICANTE', 'ESPAÑA', '03440', 'asif@asif.com', '123554455555555', 1, 'Nombre Completo: Asif Malik<br>\r\nDirección: C/ RAFAEL PASCUAL, 8, 1º, IBI<br>\r\nProvincia: ALICANTE<br>\r\nPais: ESPAÑA<br>\r\nCódigo Postal: 03440<br>\r\nE-mail: asif@asif.com<br>\r\nTeléfono: 123554455555555<br><br><br><br>Pago por Western Union: <table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n  <tbody><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/pantalon-chino-de-sarga.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/vaquero-skinny-destroy-azul-tallas-grandes-mujer-wz450_4_frf1.jpg\" width=\"50px\" ></a></td><td>Pantalón chino de sarga<br></td><td>1</td><td>283,14 </td></tr><tr><td bgcolor=\"#fe980f\"></td><td bgcolor=\"#fe980f\">Producto</td><td bgcolor=\"#fe980f\">Cantidad</td><td bgcolor=\"#fe980f\">Total</td><tr><td><a href=\"http://localhost:8080/PHP/public/TiendaOnline/hombres/bermudas-tipo-chino-con-estampado.html\">\r\n	<img src=\"http://localhost:8080/PHP/public/TiendaOnline/images/productos/bermudas-tipo-chino-cinturon-beige-hombre-xl448_1_frf1.jpg\" width=\"50px\" ></a></td><td>Bermudas tipo chino con estampado<br></td><td>28</td><td>813,12 </td></tr></tbody></table><table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"10\">\r\n     <tbody>\r\n       <tr>\r\n         <td width=\"61%\">&nbsp;</td>\r\n         <td width=\"23%\">SubTotal</td>\r\n         <td width=\"16%\">906,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Impuesto</td>\r\n         <td>190,26 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Transporte</td>\r\n         <td>0,00 </td>\r\n       </tr>\r\n       <tr>\r\n         <td>&nbsp;</td>\r\n         <td>Total</td>\r\n         <td>1096,26 </td>\r\n       </tr>\r\n     </tbody>\r\n   </table>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblconfiguracion`
--

CREATE TABLE `tblconfiguracion` (
  `idConfiguracion` int(11) NOT NULL,
  `strTelefono` varchar(50) NOT NULL,
  `strEmail` varchar(50) NOT NULL,
  `strLogo` varchar(50) NOT NULL,
  `intMarcas` int(11) NOT NULL,
  `intImpuesto` int(11) NOT NULL,
  `strPAYPAL_url` varchar(200) DEFAULT NULL,
  `strPAYPAL_email` varchar(100) DEFAULT NULL,
  `strCAIXA_url` varchar(200) DEFAULT NULL,
  `strCAIXA_fuc` varchar(100) DEFAULT NULL,
  `strCAIXA_terminal` varchar(10) DEFAULT NULL,
  `strCAIXA_version` varchar(100) DEFAULT NULL,
  `strCAIXA_clave` varchar(100) DEFAULT NULL,
  `strSANTANDER_url` varchar(200) DEFAULT NULL,
  `strSANTANDER_merchantid` varchar(100) DEFAULT NULL,
  `strSANTANDER_secret` varchar(100) DEFAULT NULL,
  `strSANTANDER_account` varchar(100) DEFAULT NULL,
  `intTransferencia` int(11) DEFAULT NULL,
  `intPaypal` int(11) DEFAULT NULL,
  `intCaixa` int(11) DEFAULT NULL,
  `intSantander` int(11) DEFAULT NULL,
  `strURL` varchar(100) DEFAULT NULL,
  `dblDescuento` double DEFAULT NULL,
  `strEmailEnvios` varchar(50) DEFAULT NULL,
  `strPassEMailEnvios` varchar(50) DEFAULT NULL,
  `strServidorCorreo` varchar(50) DEFAULT NULL,
  `intMostrarZona` int(11) DEFAULT '1',
  `intMostrarZonaImpuesto` int(11) DEFAULT '1',
  `intMostrarSlider` int(11) DEFAULT '1',
  `intWestern` int(11) DEFAULT '1',
  `intMoneyGram` int(11) DEFAULT '1',
  `intDestinoCompra` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblconfiguracion`
--

INSERT INTO `tblconfiguracion` (`idConfiguracion`, `strTelefono`, `strEmail`, `strLogo`, `intMarcas`, `intImpuesto`, `strPAYPAL_url`, `strPAYPAL_email`, `strCAIXA_url`, `strCAIXA_fuc`, `strCAIXA_terminal`, `strCAIXA_version`, `strCAIXA_clave`, `strSANTANDER_url`, `strSANTANDER_merchantid`, `strSANTANDER_secret`, `strSANTANDER_account`, `intTransferencia`, `intPaypal`, `intCaixa`, `intSantander`, `strURL`, `dblDescuento`, `strEmailEnvios`, `strPassEMailEnvios`, `strServidorCorreo`, `intMostrarZona`, `intMostrarZonaImpuesto`, `intMostrarSlider`, `intWestern`, `intMoneyGram`, `intDestinoCompra`) VALUES
(1, '+34 620023161', 'asif@gmail.com', 'logo.png', 1, 1, 'https://www.sandbox.paypal.com/cgi-bin/webscr', 'blade_asif@hotmail.com', NULL, '22222', NULL, NULL, NULL, NULL, '356345634222', '42353459834urfj3hf3kijf', 'rrr', 1, 1, 0, 0, 'http://localhost:8080/PHP/public/TiendaOnline', 0, NULL, NULL, NULL, 1, 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbldeseo`
--

CREATE TABLE `tbldeseo` (
  `idDeseo` int(11) NOT NULL,
  `refUsuario` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbldeseo`
--

INSERT INTO `tbldeseo` (`idDeseo`, `refUsuario`, `refProducto`) VALUES
(11, 5, 8),
(12, 5, 1),
(13, 5, 2),
(14, 5, 3),
(15, 5, 4),
(16, 5, 6),
(17, 45, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblidioma`
--

CREATE TABLE `tblidioma` (
  `idIdioma` int(11) NOT NULL,
  `strIdioma` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblidioma`
--

INSERT INTO `tblidioma` (`idIdioma`, `strIdioma`) VALUES
(1, 'Español'),
(2, 'Inglés'),
(3, 'Francés'),
(4, 'Portugués');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblimpuesto`
--

CREATE TABLE `tblimpuesto` (
  `idImpuesto` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `dblImpuesto` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblimpuesto`
--

INSERT INTO `tblimpuesto` (`idImpuesto`, `strNombre`, `dblImpuesto`) VALUES
(1, 'IVA 21%', 21),
(2, 'IVA 7%', 7),
(3, 'IVA 2%', 2),
(4, 'IVA 10%', 10),
(5, 'Sin impuesto', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblmarca`
--

CREATE TABLE `tblmarca` (
  `idMarca` int(11) NOT NULL,
  `strMarca` varchar(50) NOT NULL,
  `intOrden` int(11) NOT NULL,
  `intEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblmarca`
--

INSERT INTO `tblmarca` (`idMarca`, `strMarca`, `intOrden`, `intEstado`) VALUES
(1, 'Adidas', 1, 1),
(2, 'Nike', 2, 1),
(3, 'Puma', 3, 1),
(4, 'Mustang', 4, 1),
(5, 'Tommy Hilfiger', 5, 1),
(6, 'Vans', 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblmoneda`
--

CREATE TABLE `tblmoneda` (
  `idMoneda` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `dblValor` double NOT NULL,
  `intPrincipal` int(11) NOT NULL,
  `strSimbolo` varchar(10) NOT NULL,
  `strCodificacion` varchar(10) NOT NULL,
  `strHTML` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblmoneda`
--

INSERT INTO `tblmoneda` (`idMoneda`, `strNombre`, `dblValor`, `intPrincipal`, `strSimbolo`, `strCodificacion`, `strHTML`) VALUES
(1, 'Euro', 1, 1, '€', 'EUR', NULL),
(2, 'Dólar Americano', 1.06, 0, '$', 'USD', NULL),
(3, 'Dólar Canadiense', 1.43, 0, '$', 'CAD', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblopcion`
--

CREATE TABLE `tblopcion` (
  `idOpcion` int(11) NOT NULL,
  `strNombre_1` varchar(50) NOT NULL,
  `refPadre` int(11) NOT NULL DEFAULT '0',
  `intEstado` int(11) NOT NULL,
  `intOrden` int(11) NOT NULL,
  `dblIncremento` double(8,2) NOT NULL,
  `strNombre_2` varchar(50) DEFAULT NULL,
  `strNombre_3` varchar(50) DEFAULT NULL,
  `strNombre_4` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblopcion`
--

INSERT INTO `tblopcion` (`idOpcion`, `strNombre_1`, `refPadre`, `intEstado`, `intOrden`, `dblIncremento`, `strNombre_2`, `strNombre_3`, `strNombre_4`) VALUES
(1, 'Color', 0, 1, 1, 0.00, 'Colour', 'Couleur', 'Cor'),
(2, 'Talla', 0, 1, 8, 0.00, 'Talla', 'Talla', 'Talla'),
(3, 'Rojo ', 1, 1, 1, 0.00, 'Red', 'Rouge', 'Vermelha'),
(4, 'Azul', 1, 1, 2, 0.00, 'Blue', 'Bleu', 'Azul'),
(5, 'XS', 2, 1, 133, 0.00, 'XS', 'XS', 'XS'),
(6, 'L', 2, 1, 2, 0.00, 'L', 'L', 'L'),
(7, 'Verde', 1, 1, 3, 0.00, 'Green', 'Vert', 'Verde'),
(8, 'Color Super ', 0, 1, 1, 0.00, 'Colour Super ', 'Coleur Super ', 'Color Super'),
(9, 'Gris Especial', 8, 1, 1, 0.00, 'Grey Special', 'Grey Special', 'Gris Especial'),
(10, 'Beige ', 8, 1, 2, 0.00, 'Beige ', 'Beige ', 'Beige ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpreciogrupo`
--

CREATE TABLE `tblpreciogrupo` (
  `idGrupo` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `intEstado` int(11) NOT NULL,
  `refPadre` int(11) NOT NULL,
  `dblInferior` double NOT NULL,
  `dblSuperior` double NOT NULL,
  `dblCoste` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblpreciogrupo`
--

INSERT INTO `tblpreciogrupo` (`idGrupo`, `strNombre`, `intEstado`, `refPadre`, `dblInferior`, `dblSuperior`, `dblCoste`) VALUES
(1, 'Pack 10 ', 1, 0, 0, 0, 0),
(2, 'De 10 a 1000', 1, 1, 10, 1000, 15),
(3, 'De 1000 a 5000', 1, 1, 1001, 1000000, 50),
(4, 'Pack General', 1, 0, 0, 0, 0),
(5, 'De 5 a 10', 1, 4, 5, 10, 5),
(6, 'De 11 a 25', 1, 4, 11, 25, 8),
(7, 'De 26 hasta 1000000', 1, 4, 26, 1000000, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblproducto`
--

CREATE TABLE `tblproducto` (
  `idProducto` int(11) NOT NULL,
  `strNombre_1` varchar(50) NOT NULL,
  `strSEO_1` varchar(100) NOT NULL,
  `refCategoria1` int(11) NOT NULL,
  `strImagen1` varchar(250) DEFAULT NULL,
  `strDescripcion_1` text NOT NULL,
  `dblPrecio` double(8,2) NOT NULL,
  `dblPrecioAnterior` int(11) NOT NULL DEFAULT '0',
  `intEstado` int(11) NOT NULL,
  `refMarca` int(11) NOT NULL,
  `refCategoria2` int(11) NOT NULL,
  `refCategoria3` int(11) NOT NULL,
  `refCategoria4` int(11) NOT NULL,
  `refCategoria5` int(11) NOT NULL,
  `strImagen2` varchar(250) DEFAULT NULL,
  `strImagen3` varchar(250) DEFAULT NULL,
  `strImagen4` varchar(250) DEFAULT NULL,
  `strImagen5` varchar(250) DEFAULT NULL,
  `intPrincipal` int(11) NOT NULL DEFAULT '0',
  `intStock` int(11) NOT NULL DEFAULT '10',
  `refImpuesto` int(11) NOT NULL,
  `dblPeso` int(11) NOT NULL,
  `refGrupo` int(11) NOT NULL DEFAULT '0',
  `strNombre_2` varchar(50) DEFAULT NULL,
  `strDescripcion_2` text,
  `strNombre_3` varchar(50) DEFAULT NULL,
  `strDescripcion_3` text,
  `strNombre_4` varchar(50) DEFAULT NULL,
  `strDescripcion_4` text,
  `strSEO_2` varchar(100) DEFAULT NULL,
  `strSEO_3` varchar(100) DEFAULT NULL,
  `strSEO_4` varchar(100) DEFAULT NULL,
  `strTitleSEO_1` varchar(50) DEFAULT NULL,
  `strTitleSEO_2` varchar(50) DEFAULT NULL,
  `strTitleSEO_3` varchar(50) DEFAULT NULL,
  `strTitleSEO_4` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblproducto`
--

INSERT INTO `tblproducto` (`idProducto`, `strNombre_1`, `strSEO_1`, `refCategoria1`, `strImagen1`, `strDescripcion_1`, `dblPrecio`, `dblPrecioAnterior`, `intEstado`, `refMarca`, `refCategoria2`, `refCategoria3`, `refCategoria4`, `refCategoria5`, `strImagen2`, `strImagen3`, `strImagen4`, `strImagen5`, `intPrincipal`, `intStock`, `refImpuesto`, `dblPeso`, `refGrupo`, `strNombre_2`, `strDescripcion_2`, `strNombre_3`, `strDescripcion_3`, `strNombre_4`, `strDescripcion_4`, `strSEO_2`, `strSEO_3`, `strSEO_4`, `strTitleSEO_1`, `strTitleSEO_2`, `strTitleSEO_3`, `strTitleSEO_4`) VALUES
(1, 'Bermudas vaqueras tipo jogging', 'bermudas-vaqueras-tipo-jogging', 4, 'camisa-comoda-de-punto-azul-tallas-grandes-hombre-vs913_35_frf1.jpg', '<h2 id=\"productName\">Bermudas vaqueras tipo jogging</h2>', 5.00, 10, 1, 6, 6, 0, 0, 0, 'camisa-comoda-de-punto-azul-tallas-grandes-hombre-vs913_35_frf2.jpg', 'camisa-comoda-de-punto-azul-tallas-grandes-hombre-vs913_35_frf3.jpg', 'camisa-comoda-de-punto-azul-tallas-grandes-hombre-vs913_35_frf4.jpg', 'camisa-comoda-de-punto-azul-tallas-grandes-hombre-vs913_35_frf5.jpg', 1, 822, 1, 8, 1, 'Bermudas vaqueras tipo jogging', '<p><strong>asdasdas</strong></p>\r\n<p>zsdfs</p>\r\n<p>sdr</p>', 'Bermudas vaqueras tipo jogging', '<h2 id=\"productName\">Bermudas vaqueras tipo jogging</h2>', 'Bermudas vaqueras tipo jogging', '<h2 id=\"productName\">Bermudas vaqueras tipo jogging</h2>', 'bermudas-vaqueras-tipo-jogging', 'bermudas-vaqueras-tipo-jogging', 'bermudas-vaqueras-tipo-jogging', NULL, NULL, NULL, NULL),
(2, 'Bermudas tipo chino', 'bermudas-tipo-chino', 1, 'bermudas-estampadas-all-over-marron-hombre-xl478_2_frf1.jpg', '<h2><a id=\"XL481\" class=\"h2\" href=\"https://www.kiabi.es/bermudas-tipo-chino-hombre-naranja_P636108C636109\" data-ns-name=\"nshop\" data-ns-value=\"$nsvalue\" data-selenium=\"Bermudas tipo chino\" data-ref=\"Bermudas tipo chino\">Bermudas tipo chino </a></h2>', 81.00, 0, 1, 6, 12, 0, 0, 0, 'bermudas-estampadas-all-over-marron-hombre-xl478_2_frm.jpg', NULL, NULL, NULL, 1, 100, 2, 0, 4, 'Bermudas tipo chino', '<h2><a id=\"XL481\" class=\"h2\" href=\"https://www.kiabi.es/bermudas-tipo-chino-hombre-naranja_P636108C636109\" data-ns-name=\"nshop\" data-ns-value=\"$nsvalue\" data-selenium=\"Bermudas tipo chino\" data-ref=\"Bermudas tipo chino\">Bermudas tipo chino </a></h2>', 'Bermudas tipo chino', '<h2><a id=\"XL481\" class=\"h2\" href=\"https://www.kiabi.es/bermudas-tipo-chino-hombre-naranja_P636108C636109\" data-ns-name=\"nshop\" data-ns-value=\"$nsvalue\" data-selenium=\"Bermudas tipo chino\" data-ref=\"Bermudas tipo chino\">Bermudas tipo chino </a></h2>', 'Bermudas tipo chino', '<h2><a id=\"XL481\" class=\"h2\" href=\"https://www.kiabi.es/bermudas-tipo-chino-hombre-naranja_P636108C636109\" data-ns-name=\"nshop\" data-ns-value=\"$nsvalue\" data-selenium=\"Bermudas tipo chino\" data-ref=\"Bermudas tipo chino\">Bermudas tipo chino </a></h2>', 'bermudas-tipo-chino', 'bermudas-tipo-chino', 'bermudas-tipo-chino', NULL, NULL, NULL, NULL),
(3, 'Camisa slim de flores', 'camisa-slim-de-flores', 1, 'jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf1.jpg', '<h2 id=\"productName\">Camisa slim de flores</h2>', 56.00, 0, 1, 5, 6, 0, 0, 0, 'jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf2.jpg', 'jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf3.jpg', 'jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf4.jpg', 'jersey-de-algodon-eco-concepcion-marron-hombre-xe938_1_frf5.jpg', 1, 1020, 3, 0, 1, 'Camisa slim de flores', '<h2 id=\"productName\">Camisa slim de flores</h2>', 'Camisa slim de flores', '<h2 id=\"productName\">Camisa slim de flores</h2>', 'Camisa slim de flores', '<h2 id=\"productName\">Camisa slim de flores</h2>', 'camisa-slim-de-flores', 'camisa-slim-de-flores', 'camisa-slim-de-flores', NULL, NULL, NULL, NULL),
(4, 'Pantalón chino de sarga', 'pantalon-chino-de-sarga', 1, 'vaquero-skinny-destroy-azul-tallas-grandes-mujer-wz450_4_frf1.jpg', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Pantal&oacute;n chino de sarga de algod&oacute;n el&aacute;stica</h2>\r\n</div>', 234.00, 0, 1, 6, 11, 3, 0, 0, 'vaquero-skinny-destroy-azul-tallas-grandes-mujer-wz450_4_frf2.jpg', 'vaquero-skinny-destroy-azul-tallas-grandes-mujer-wz450_4_frf3.jpg', NULL, NULL, 1, 10, 1, 0, 0, 'Pantalón chino de sarga', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Pantal&oacute;n chino de sarga de algod&oacute;n el&aacute;stica</h2>\r\n</div>', 'Pantalón chino de sarga', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Pantal&oacute;n chino de sarga de algod&oacute;n el&aacute;stica</h2>\r\n</div>', 'Pantalón chino de sarga', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Pantal&oacute;n chino de sarga de algod&oacute;n el&aacute;stica</h2>\r\n</div>', 'pantalon-chino-de-sarga', 'pantalon-chino-de-sarga', 'pantalon-chino-de-sarga', NULL, NULL, NULL, NULL),
(5, 'Pantalón chino skinny', 'pantalon-chino-skinny', 1, 'pantalon-chino-de-sarga-de-algodon-elastica-azul-poseidon-hombre-vj070_65_frf1.jpg', '<h2 id=\"productName\">Pantal&oacute;n chino skinny</h2>', 56.80, 0, 1, 6, 5, 11, 5, 0, 'pantalon-chino-de-sarga-de-algodon-elastica-azul-poseidon-hombre-vj070_65_frf2.jpg', 'pantalon-chino-de-sarga-de-algodon-elastica-azul-poseidon-hombre-vj070_65_frf3.jpg', 'pantalon-chino-de-sarga-de-algodon-elastica-azul-poseidon-hombre-vj070_65_frf4.jpg', 'pantalon-chino-de-sarga-de-algodon-elastica-azul-poseidon-hombre-vj070_65_frf5.jpg', 1, 1064, 1, 0, 4, 'Pantalón chino skinny', '<h2 id=\"productName\">Pantal&oacute;n chino skinny</h2>', 'Pantalón chino skinny', '<h2 id=\"productName\">Pantal&oacute;n chino skinny</h2>', 'Pantalón chino skinny', '<h2 id=\"productName\">Pantal&oacute;n chino skinny</h2>', 'pantalon-chino-skinny', 'pantalon-chino-skinny', 'pantalon-chino-skinny', NULL, NULL, NULL, NULL),
(6, 'Camiseta bordada', 'camiseta-bordada', 1, 'camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf1.jpg', '<h2 id=\"productName\">Camiseta bordada</h2>\r\n<h2 id=\"productName\">Camiseta bordada</h2>', 50.99, 0, 1, 6, 6, 4, 0, 0, 'camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf3.jpg', 'camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf4.jpg', 'camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf5.jpg', 'camiseta-bordada-azul-mujer-talla-34-a-48-xq344_3_frf6.jpg', 1, 1030, 1, 10, 4, 'Camiseta bordada', '<h2 id=\"productName\">Camiseta bordada</h2>\r\n<h2 id=\"productName\">Camiseta bordada</h2>', 'Camiseta bordada', '<h2 id=\"productName\">Camiseta bordada</h2>', 'Camiseta bordada', '<h2 id=\"productName\">Camiseta bordada</h2>', 'camiseta-bordada', 'camiseta-bordada', 'camiseta-bordada', NULL, NULL, NULL, NULL),
(7, 'Bermudas tipo chino con estampado', 'bermudas-tipo-chino-con-estampado', 1, 'bermudas-tipo-chino-cinturon-beige-hombre-xl448_1_frf1.jpg', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Bermudas tipo chino con estampado all over</h2>\r\n</div>', 24.00, 0, 1, 0, 12, 0, 0, 0, 'bermudas-tipo-chino-cinturon-beige-hombre-xl448_1_frf2.jpg', 'bermudas-tipo-chino-cinturon-beige-hombre-xl448_1_frf3.jpg', 'bermudas-tipo-chino-cinturon-beige-hombre-xl448_1_frf4.jpg', 'bermudas-tipo-chino-cinturon-beige-hombre-xl448_1_frm.jpg', 1, 1064, 1, 0, 0, 'Bermudas tipo chino con estampado', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Bermudas tipo chino con estampado all over</h2>\r\n</div>', 'Bermudas tipo chino con estampado', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Bermudas tipo chino con estampado all over</h2>\r\n</div>', 'Bermudas tipo chino con estampado', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Bermudas tipo chino con estampado all over</h2>\r\n</div>', 'bermudas-tipo-chino-con-estampado', 'bermudas-tipo-chino-con-estampado', 'bermudas-tipo-chino-con-estampado', NULL, NULL, NULL, NULL),
(8, 'Camiseta de algodón estampada', 'camiseta-de-algodon-estampada', 3, 'camiseta-de-algodon-estampada-rosa-mujer-talla-34-a-48-xj589_3_frf1.jpg', '<p>&nbsp;</p>\r\n<h2 id=\"productName\">Camiseta de algod&oacute;n estampada</h2>', 20.36, 0, 1, 3, 6, 4, 0, 0, 'camiseta-de-algodon-estampada-rosa-mujer-talla-34-a-48-xj589_3_frf2.jpg', 'camiseta-de-algodon-estampada-rosa-mujer-talla-34-a-48-xj589_3_frf3.jpg', 'camiseta-de-algodon-estampada-rosa-mujer-talla-34-a-48-xj589_3_frm.jpg', NULL, 1, 1020, 1, 0, 0, 'Camiseta de algodón estampada', '<p>&nbsp;</p>\r\n<h2 id=\"productName\">Camiseta de algod&oacute;n estampada</h2>', 'Camiseta de algodón estampada', '<p>&nbsp;</p>\r\n<h2 id=\"productName\">Camiseta de algod&oacute;n estampada</h2>', 'Camiseta de algodón estampada', '<p>&nbsp;</p>\r\n<h2 id=\"productName\">Camiseta de algod&oacute;n estampada</h2>', 'camiseta-de-algodon-estampada', 'camiseta-de-algodon-estampada', 'camiseta-de-algodon-estampada', NULL, NULL, NULL, NULL),
(9, 'Camiseta colorblock \'eco-concepción\'', 'camiseta-colorblock-eco-concepcion-', 6, 'camiseta-colorblock-eco-concepcion-naranja-hombre-xk468_2_frf1.jpg', '<h2 id=\"productName\">Camiseta colorblock \'eco-concepci&oacute;n\'</h2>', 125.00, 0, 1, 0, 1, 6, 0, 0, NULL, NULL, NULL, NULL, 1, 200, 1, 0, 0, 'Camiseta colorblock \'eco-concepción\'', '<h2 id=\"productName\">Camiseta colorblock \'eco-concepci&oacute;n\'</h2>', 'Camiseta colorblock \'eco-concepción\'', '<h2 id=\"productName\">Camiseta colorblock \'eco-concepci&oacute;n\'</h2>', 'Camiseta colorblock \'eco-concepción\'', '<h2 id=\"productName\">Camiseta colorblock \'eco-concepci&oacute;n\'</h2>', 'camiseta-colorblock-eco-concepcion-', 'camiseta-colorblock-eco-concepcion-', 'camiseta-colorblock-eco-concepcion-', NULL, NULL, NULL, NULL),
(10, 'Americana skinny', 'americana-skinny', 1, 'polo-slim-con-cuello-mao-azul-hombre-xh543_2_frf1.jpg', '<p>Americana skinny</p>', 123.00, 0, 0, 0, 5, 0, 0, 0, 'polo-slim-con-cuello-mao-azul-hombre-xh543_2_frf2.jpg', 'polo-slim-con-cuello-mao-azul-hombre-xh543_2_frf3.jpg', 'polo-slim-con-cuello-mao-azul-hombre-xh543_2_frf4.jpg', 'polo-slim-con-cuello-mao-azul-hombre-xh543_2_frf5.jpg', 1, 1023, 1, 0, 0, 'Americana skinny', '<p>Americana skinny</p>', 'Americana skinny', '<p>Americana skinny</p>', 'Americana skinny', '<p>Americana skinny</p>', 'americana-skinny', 'americana-skinny', 'americana-skinny', NULL, NULL, NULL, NULL),
(11, 'Camiseta \'Kodak\'', 'camiseta-kodak-', 1, '1.jpg', '<p>Camiseta \'Kodak\'</p>', 6.00, 10, 1, 6, 6, 0, 0, 0, '2.jpg', '3.jpg', '4.jpg', '5.jpg', 1, 856, 5, 8, 0, 'Camiseta \'Kodak\'', '<p>Camiseta \'Kodak\'</p>', 'Camiseta \'Kodak\'', '<p>Camiseta \'Kodak\'</p>', 'Camiseta \'Kodak\'', '<p>Camiseta \'Kodak\'</p>', 'camiseta-kodak-', 'camiseta-kodak-', 'camiseta-kodak-', NULL, NULL, NULL, NULL),
(12, 'Pelele con efecto acolchado', 'pelele-con-efecto-acolchado', 1, 'pelele-con-efecto-acolchado-negro-bebe-nino-xi643_1_frf1.jpg', '<h2 id=\"productName\">Pelele con efecto acolchado</h2>', 15.00, 25, 1, 6, 3, 0, 0, 0, 'pelele-con-efecto-acolchado-negro-bebe-nino-xi643_1_frf2.jpg', 'pelele-con-efecto-acolchado-negro-bebe-nino-xi643_1_frm.jpg', NULL, NULL, 1, 869, 5, 8, 0, 'Pelele con efecto acolchado', '<h2 id=\"productName\">Pelele con efecto acolchado</h2>', 'Pelele con efecto acolchado', '<h2 id=\"productName\">Pelele con efecto acolchado</h2>', 'Pelele con efecto acolchado', '<h2 id=\"productName\">Pelele con efecto acolchado</h2>', 'pelele-con-efecto-acolchado', 'pelele-con-efecto-acolchado', 'pelele-con-efecto-acolchado', NULL, NULL, NULL, NULL),
(13, 'Bermudas tipo chino + cinturón', 'bermudas-tipo-chino-cinturon', 1, 'camiseta-slim-bicolor-eco-concepcion-blanco-hombre-xh536_5_frf1.jpg', '<p>Bermudas tipo chino + cintur&oacute;n</p>', 25.00, 58, 1, 6, 12, 0, 0, 0, 'camiseta-slim-bicolor-eco-concepcion-blanco-hombre-xh536_5_frf2.jpg', 'camiseta-slim-bicolor-eco-concepcion-blanco-hombre-xh536_5_frf3.jpg', 'camiseta-slim-bicolor-eco-concepcion-blanco-hombre-xh536_5_frf4.jpg', 'camiseta-slim-bicolor-eco-concepcion-blanco-hombre-xh536_5_frm.jpg', 1, 8855, 5, 8, 0, 'Bermudas tipo chino + cinturón', '<p>Bermudas tipo chino + cintur&oacute;n</p>', 'Bermudas tipo chino + cinturón', '<p>Bermudas tipo chino + cintur&oacute;n</p>', 'Bermudas tipo chino + cinturón', '<p>Bermudas tipo chino + cintur&oacute;n</p>', 'bermudas-tipo-chino-cinturon', 'bermudas-tipo-chino-cinturon', 'bermudas-tipo-chino-cinturon', NULL, NULL, NULL, NULL),
(14, 'Juego de cama individual \'Minnie\'', 'juego-de-cama-individual-minnie-', 5, 'juego-de-cama-individual-minnie-rosa-hogar-xp109_1_frf1.jpg', '<p>Juego de cama individual \'Minnie\'</p>', 12.00, 25, 1, 6, 3, 0, 0, 0, 'juego-de-cama-individual-minnie-rosa-hogar-xp109_1_frm.jpg', 'juego-de-cama-individual-minnie-rosa-hogar-xp109_1_frm.jpg', NULL, NULL, 1, 8, 3, 8, 0, 'Juego de cama individual \'Minnie\'', '<p>Juego de cama individual \'Minnie\'</p>', 'Juego de cama individual \'Minnie\'', '<p>Juego de cama individual \'Minnie\'</p>', 'Juego de cama individual \'Minnie\'', '<p>Juego de cama individual \'Minnie\'</p>', 'juego-de-cama-individual-minnie-', 'juego-de-cama-individual-minnie-', 'juego-de-cama-individual-minnie-', NULL, NULL, NULL, NULL),
(15, 'Vestido cruzado', 'vestido-cruzado', 4, 'vestido-cruzado-rosa-mujer-talla-34-a-48-xj633_5_frf1.jpg', '<p>Vestido cruzado</p>', 10.00, 15, 1, 6, 5, 0, 0, 0, 'vestido-cruzado-rosa-mujer-talla-34-a-48-xj633_5_frm.jpg', '', NULL, NULL, 1, 869, 1, 8, 0, 'Vestido cruzado', '<p>Vestido cruzado</p>', 'Vestido cruzado', '<p>Vestido cruzado</p>', 'Vestido cruzado', '<p>Vestido cruzado</p>', 'vestido-cruzado', 'vestido-cruzado', 'vestido-cruzado', NULL, NULL, NULL, NULL),
(16, 'Camisa', 'camisa', 1, 'camisa-azul-tallas-grandes-hombre-xh913_6_frf1.jpg', '<p>Camisa</p>', 20.00, 0, 1, 1, 6, 3, 0, 0, 'camisa-azul-tallas-grandes-hombre-xh913_6_frf2.jpg', 'camisa-azul-tallas-grandes-hombre-xh913_6_frf3.jpg', 'camisa-azul-tallas-grandes-hombre-xh913_6_frf4.jpg', 'camisa-azul-tallas-grandes-hombre-xh913_6_frm.jpg', 1, 869, 1, 8, 0, 'Camisa', '<p>Camisa</p>', 'Camisa', '<p>Camisa</p>', 'Camisa', '<p>Camisa</p>', 'camisa', 'camisa', 'camisa', NULL, NULL, NULL, NULL),
(17, 'Vestido tipo camiseta', 'vestido-tipo-camiseta', 4, 'vestido-tipo-camiseta-negro-tallas-grandes-mujer-xh740_3_frf1.jpg', '<h2 id=\"productName\">Vestido tipo camiseta</h2>', 15.00, 0, 1, 4, 3, 0, 0, 0, 'vestido-tipo-camiseta-negro-tallas-grandes-mujer-xh740_3_frf2.jpg', 'vestido-tipo-camiseta-negro-tallas-grandes-mujer-xh740_3_frf3.jpg', 'vestido-tipo-camiseta-negro-tallas-grandes-mujer-xh740_3_frm.jpg', NULL, 1, 256, 1, 8, 0, 'Vestido tipo camiseta', '<h2 id=\"productName\">Vestido tipo camiseta</h2>', 'Vestido tipo camiseta', '<h2 id=\"productName\">Vestido tipo camiseta</h2>', 'Vestido tipo camiseta', '<h2 id=\"productName\">Vestido tipo camiseta</h2>', 'vestido-tipo-camiseta', 'vestido-tipo-camiseta', 'vestido-tipo-camiseta', NULL, NULL, NULL, NULL),
(18, 'Vestido vaporoso de viscosa', 'vestido-vaporoso-de-viscosa', 4, 'vestido-vaporoso-de-viscosa-rojo-tallas-grandes-mujer-xm103_2_frf1.jpg', '<p>Vestido vaporoso de viscosa</p>', 56.00, 0, 1, 2, 5, 0, 0, 0, 'vestido-vaporoso-de-viscosa-rojo-tallas-grandes-mujer-xm103_2_frm.jpg', 'vestido-vaporoso-de-viscosa-rojo-tallas-grandes-mujer-xm103_2_frm.jpg', NULL, NULL, 1, 858, 1, 8, 0, 'Vestido vaporoso de viscosa', '<p>Vestido vaporoso de viscosa</p>', 'Vestido vaporoso de viscosa', '<p>Vestido vaporoso de viscosa</p>', 'Vestido vaporoso de viscosa', '<p>Vestido vaporoso de viscosa</p>', 'vestido-vaporoso-de-viscosa', 'vestido-vaporoso-de-viscosa', 'vestido-vaporoso-de-viscosa', NULL, NULL, NULL, NULL),
(19, 'Digital Printed Lawn Fabric - 3PC', 'digital-printed-lawn-fabric-3pc', 4, '42001103_3_.jpg', '<h1 class=\"page-title\"><span class=\"base\" data-ui-id=\"page-title-wrapper\">Digital Printed Lawn Fabric - 3PC</span></h1>', 85.00, 0, 0, 3, 5, 0, 0, 0, '42001103_5_.jpg', '42001103_6_.jpg', '42001103_7_.jpg', NULL, 0, 854, 1, 8, 0, 'Digital Printed Lawn Fabric - 3PC', '<h1 class=\"page-title\"><span class=\"base\" data-ui-id=\"page-title-wrapper\">Digital Printed Lawn Fabric - 3PC</span></h1>', 'Digital Printed Lawn Fabric - 3PC', '<h1 class=\"page-title\"><span class=\"base\" data-ui-id=\"page-title-wrapper\">Digital Printed Lawn Fabric - 3PC</span></h1>', 'Digital Printed Lawn Fabric - 3PC', '<h1 class=\"page-title\"><span class=\"base\" data-ui-id=\"page-title-wrapper\">Digital Printed Lawn Fabric - 3PC</span></h1>', 'digital-printed-lawn-fabric-3pc', 'digital-printed-lawn-fabric-3pc', 'digital-printed-lawn-fabric-3pc', NULL, NULL, NULL, NULL),
(20, 'Camiseta con mensaje bordado', 'camiseta-con-mensaje-bordado', 4, 'camiseta-con-mensaje-bordado-blanco-nieve-tallas-grandes-mujer-xq484_1_frf1.jpg', '<p>Camiseta con mensaje bordado</p>', 45.00, 0, 1, 2, 5, 0, 0, 0, 'camiseta-con-mensaje-bordado-blanco-nieve-tallas-grandes-mujer-xq484_1_frf2.jpg', 'camiseta-con-mensaje-bordado-blanco-nieve-tallas-grandes-mujer-xq484_1_frf3.jpg', NULL, NULL, 1, 885, 1, 8, 0, 'Camiseta con mensaje bordado', '<p>Camiseta con mensaje bordado</p>', 'Camiseta con mensaje bordado', '<p>Camiseta con mensaje bordado</p>', 'Camiseta con mensaje bordado', '<p>Camiseta con mensaje bordado</p>', 'camiseta-con-mensaje-bordado', 'camiseta-con-mensaje-bordado', 'camiseta-con-mensaje-bordado', NULL, NULL, NULL, NULL),
(21, 'Pantalón chino regular', 'pantalon-chino-regular', 1, 'pantalon-chino-regular-azul-hombre-xh472_2_frf1.jpg', '<p>Pantal&oacute;n chino regular</p>', 5554.00, 10500, 1, 6, 11, 0, 0, 0, 'pantalon-chino-regular-azul-hombre-xh472_2_frf2.jpg', 'pantalon-chino-regular-azul-hombre-xh472_2_frf3.jpg', 'pantalon-chino-regular-azul-hombre-xh472_2_frf4.jpg', NULL, 1, 8, 5, 8, 0, 'Pantalón chino regular', '<p>Pantal&oacute;n chino regular</p>', 'Pantalón chino regular', '<p>Pantal&oacute;n chino regular</p>', 'Pantalón chino regular', '<p>Pantal&oacute;n chino regular</p>', 'pantalon-chino-regular', 'pantalon-chino-regular', 'pantalon-chino-regular', NULL, NULL, NULL, NULL),
(24, 'Polo slim con micromotivo', 'polo-slim-con-micromotivo', 1, 'polo-slim-con-micromotivo-azul-hombre-xk279_4_frf1.jpg', '<h2 id=\"productName\">Polo slim con micromotivo</h2>', 13.56, 0, 1, 1, 6, 4, 0, 0, 'polo-slim-con-micromotivo-azul-hombre-xk279_4_frf2.jpg', 'polo-slim-con-micromotivo-azul-hombre-xk279_4_frf3.jpg', NULL, NULL, 1, 566, 1, 0, 0, 'Polo slim con micromotivo', NULL, 'Polo slim con micromotivo', NULL, 'Polo slim con micromotivo', NULL, 'polo-slim-con-micromotivo', 'polo-slim-con-micromotivo', 'polo-slim-con-micromotivo', NULL, NULL, NULL, NULL),
(25, 'Pantalón vaporoso', 'pantalon-vaporoso', 4, 'pantalon-vaporoso-amarillo-mujer-talla-34-a-48-xh619_8_frf1.jpg', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Pantal&oacute;n vaporoso</h2>\r\n</div>', 25.36, 0, 1, 2, 11, 0, 0, 0, 'pantalon-vaporoso-amarillo-mujer-talla-34-a-48-xh619_8_frf2.jpg', 'pantalon-vaporoso-amarillo-mujer-talla-34-a-48-xh619_8_frf3.jpg', 'pantalon-vaporoso-amarillo-mujer-talla-34-a-48-xh619_8_frf4.jpg', NULL, 1, 658, 1, 0, 0, 'Pantalón vaporoso', '<p>&nbsp;</p>\r\n<div class=\"product-identity\">\r\n<h2 id=\"productName\">Pantal&oacute;n vaporoso</h2>\r\n</div>', 'Pantalón vaporoso', '<p>Pantal&oacute;n vaporoso</p>', 'Pantalón vaporoso', '<p>Pantal&oacute;n vaporoso</p>', 'pantalon-vaporoso', 'pantalon-vaporoso', 'pantalon-vaporoso', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblproductocaracteristica`
--

CREATE TABLE `tblproductocaracteristica` (
  `idProductocaracteristica` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL,
  `refCaracteristica` int(11) NOT NULL,
  `refSeleccionada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblproductocaracteristica`
--

INSERT INTO `tblproductocaracteristica` (`idProductocaracteristica`, `refProducto`, `refCaracteristica`, `refSeleccionada`) VALUES
(16, 8, 1, 13),
(17, 8, 2, 8),
(18, 8, 3, 15),
(22, 11, 1, 12),
(23, 11, 2, 7),
(24, 1, 1, 14),
(25, 1, 2, 7),
(26, 1, 3, 15),
(27, 12, 1, 12),
(28, 12, 2, 7),
(29, 13, 1, 12),
(30, 13, 2, 7),
(31, 14, 1, 12),
(32, 14, 2, 7),
(33, 15, 1, 12),
(34, 15, 2, 7),
(39, 5, 1, 13),
(40, 5, 2, 10),
(41, 5, 3, 16),
(42, 4, 4, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblproductoopcion`
--

CREATE TABLE `tblproductoopcion` (
  `idProductoOpcion` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL,
  `refOpcion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblproductoopcion`
--

INSERT INTO `tblproductoopcion` (`idProductoOpcion`, `refProducto`, `refOpcion`) VALUES
(7, 2, 1),
(8, 1, 8),
(9, 1, 2),
(10, 5, 1),
(11, 5, 2),
(12, 5, 8),
(13, 2, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblproductovisita`
--

CREATE TABLE `tblproductovisita` (
  `idProductovisita` int(11) NOT NULL,
  `refUsuario` int(11) NOT NULL,
  `refProducto` int(11) NOT NULL,
  `fchFecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblproductovisita`
--

INSERT INTO `tblproductovisita` (`idProductovisita`, `refUsuario`, `refProducto`, `fchFecha`) VALUES
(82, 45, 1, '2020-05-22 17:21:54'),
(83, 45, 1, '2020-05-22 17:22:28'),
(84, 45, 6, '2020-05-22 17:39:02'),
(85, 5, 6, '2020-05-22 18:43:12'),
(86, 5, 6, '2020-05-22 18:43:34'),
(87, 5, 6, '2020-05-22 18:43:35'),
(88, 5, 6, '2020-05-22 18:44:26'),
(89, 5, 5, '2020-05-22 18:44:28'),
(90, 5, 5, '2020-05-22 18:47:18'),
(91, 5, 5, '2020-05-22 18:47:30'),
(92, 5, 6, '2020-05-22 18:47:32'),
(93, 5, 6, '2020-05-22 18:47:45'),
(94, 5, 6, '2020-05-22 18:51:39'),
(95, 5, 6, '2020-05-22 18:52:46'),
(96, 5, 6, '2020-05-22 18:56:12'),
(97, 5, 6, '2020-05-22 18:56:17'),
(98, 5, 6, '2020-05-22 18:56:41'),
(99, 5, 2, '2020-05-23 21:06:28'),
(100, 5, 3, '2020-05-23 21:06:47'),
(101, 5, 1, '2020-05-23 21:25:14'),
(102, 5, 1, '2020-05-23 21:27:01'),
(103, 5, 1, '2020-05-23 21:33:58'),
(104, 5, 6, '2020-05-23 21:34:16'),
(105, 5, 9, '2020-05-23 21:34:49'),
(106, 5, 4, '2020-05-23 21:35:04'),
(107, 45, 19, '2020-05-24 21:56:46'),
(108, 5, 6, '2020-05-24 22:54:20'),
(109, 5, 4, '2020-05-24 22:54:26'),
(110, 5, 15, '2020-05-24 22:54:31'),
(111, 5, 25, '2020-05-25 17:29:59'),
(112, 45, 1, '2020-05-25 17:39:04'),
(113, 45, 6, '2020-05-25 17:39:11'),
(114, 45, 4, '2020-05-25 17:39:16'),
(115, 45, 1, '2020-05-25 20:48:16'),
(116, 45, 3, '2020-05-25 20:50:26'),
(117, 45, 3, '2020-05-25 20:50:49'),
(118, 45, 3, '2020-05-25 20:51:04'),
(119, 45, 3, '2020-05-25 20:51:27'),
(120, 45, 3, '2020-05-25 20:51:36'),
(121, 45, 3, '2020-05-25 20:52:31'),
(122, 45, 3, '2020-05-25 20:52:54'),
(123, 45, 6, '2020-05-25 20:54:01'),
(124, 45, 15, '2020-05-25 20:54:52'),
(125, 45, 15, '2020-05-25 20:55:00'),
(126, 5, 1, '2020-05-25 21:59:24'),
(127, 45, 4, '2020-05-26 15:14:59'),
(128, 45, 4, '2020-05-26 15:15:14'),
(129, 45, 4, '2020-05-26 15:15:31'),
(130, 45, 2, '2020-05-26 15:17:19'),
(131, 45, 3, '2020-05-26 15:17:23'),
(132, 45, 6, '2020-05-26 15:17:27'),
(133, 45, 6, '2020-05-26 15:17:30'),
(134, 45, 8, '2020-05-26 15:17:32'),
(135, 45, 15, '2020-05-26 15:17:35'),
(136, 45, 11, '2020-05-26 15:18:49'),
(137, 45, 6, '2020-05-26 15:20:07'),
(138, 45, 1, '2020-05-26 15:25:37'),
(139, 45, 1, '2020-05-26 16:26:30'),
(140, 45, 4, '2020-05-26 16:44:07'),
(141, 49, 5, '2020-05-27 19:34:58'),
(142, 49, 3, '2020-05-27 19:39:42'),
(143, 45, 15, '2020-05-28 20:20:58'),
(144, 45, 7, '2020-05-28 20:21:15'),
(145, 45, 12, '2020-05-28 20:31:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblslider`
--

CREATE TABLE `tblslider` (
  `idSlider` int(11) NOT NULL,
  `strTexto_1` text NOT NULL,
  `strImagen` varchar(100) NOT NULL,
  `strLink_1` varchar(200) DEFAULT NULL,
  `intEstado` int(11) NOT NULL,
  `intOrden` int(11) NOT NULL,
  `strTexto_2` text,
  `strLink_2` varchar(200) DEFAULT NULL,
  `strTexto_3` text,
  `strLink_3` varchar(200) DEFAULT NULL,
  `strTexto_4` text,
  `strLink_4` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblslider`
--

INSERT INTO `tblslider` (`idSlider`, `strTexto_1`, `strImagen`, `strLink_1`, `intEstado`, `intOrden`, `strTexto_2`, `strLink_2`, `strTexto_3`, `strLink_3`, `strTexto_4`, `strLink_4`) VALUES
(1, '<h1>Feliz San Valentin</h1>\r\n<h2>Con las mejores intenciones.</h2>\r\n<p>Aqui encontraras todo lo que necesitas.</p>', 'img_lmonton.jpg', '', 1, 1, '<h1>Feliz San Valentin</h1>\r\n<h2>Con las mejores intenciones.</h2>\r\n<p>Aqui encontraras todo lo que necesitas.</p>', NULL, '<h1>Feliz San Valentin</h1>\r\n<h2>Con las mejores intenciones.</h2>\r\n<p>Aqui encontraras todo lo que necesitas.</p>', NULL, '<h1>Feliz San Valentin</h1>\r\n<h2>Con las mejores intenciones.</h2>\r\n<p>Aqui encontraras todo lo que necesitas.</p>', NULL),
(2, '<h1>Ahora es el momento</h1>\r\n<h2>Por fin ropa de lujo</h2>\r\n<p>Las mejores caracter&iacute;sticas.</p>', 'dreamstime.jpg', '', 1, 2, '<h1>Ahora es el momento</h1>\r\n<h2>Por fin ropa de lujo</h2>\r\n<p>Las mejores caracter&iacute;sticas.</p>', NULL, '<h1>Ahora es el momento</h1>\r\n<h2>Por fin ropa de lujo</h2>\r\n<p>Las mejores caracter&iacute;sticas.</p>', NULL, '<h1>Ahora es el momento</h1>\r\n<h2>Por fin ropa de lujo</h2>\r\n<p>Las mejores caracter&iacute;sticas.</p>', NULL),
(3, '<h1>Lo mejor de lo mejor</h1>\r\n<h2>Lo ultimo en oulet</h2>\r\n<p>Cons&uacute;ltenos sin compromiso.</p>', 'ZONA-OUTLET.png', '/outlet/', 1, 3, '<h1>Lo mejor de lo mejor</h1>\r\n<h2>Lo ultimo en oulet</h2>\r\n<p>Cons&uacute;ltenos sin compromiso.</p>', '/outlet/', '<h1>Lo mejor de lo mejor</h1>\r\n<h2>Lo ultimo en oulet</h2>\r\n<p>Cons&uacute;ltenos sin compromiso.</p>', '/outlet/', '<h1>Lo mejor de lo mejor</h1>\r\n<h2>Lo ultimo en oulet</h2>\r\n<p>Cons&uacute;ltenos sin compromiso.</p>', '/outlet/'),
(4, '<h1>Novedades</h1>\r\n<h2>Novedades</h2>\r\n<p>Novedades</p>', 'novedades-mujer-hombre-inside.jpg', '', 1, 4, '<h1>Novedades</h1>\r\n<h2>Novedades</h2>\r\n<p>Novedades</p>', NULL, '<h1>Novedades</h1>\r\n<h2>Novedades</h2>\r\n<p>Novedades</p>', NULL, '<h1>Novedades</h1>\r\n<h2>Novedades</h2>\r\n<p>Novedades</p>', NULL),
(5, '<p>Escribir Texto</p>', 'BATOI.png', NULL, 0, 1, '<p>Escribir Texto</p>', NULL, '<p>Escribir Texto</p>', NULL, '<p>Escribir Texto</p>', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuario`
--

CREATE TABLE `tblusuario` (
  `idUsuario` int(11) NOT NULL,
  `strEmail` varchar(50) DEFAULT NULL,
  `strPassword` varchar(50) DEFAULT NULL,
  `strNombre` varchar(30) DEFAULT NULL,
  `intNivel` int(11) NOT NULL DEFAULT '0',
  `intEstado` int(11) NOT NULL DEFAULT '1',
  `strImagen` varchar(50) DEFAULT NULL,
  `fchAlta` datetime NOT NULL,
  `strCookie` varchar(50) DEFAULT NULL,
  `strRecuperar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblusuario`
--

INSERT INTO `tblusuario` (`idUsuario`, `strEmail`, `strPassword`, `strNombre`, `intNivel`, `intEstado`, `strImagen`, `fchAlta`, `strCookie`, `strRecuperar`) VALUES
(5, 'p@p.com', '1b93a10a12e060b8146b820e4e279cdb', 'Felipe', 0, 1, NULL, '2017-01-01 00:00:00', NULL, '8f5ae78da379aeeade0beceb5d0c8f'),
(10, 'asif@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Asif Malik', 1, 1, NULL, '2017-01-01 00:00:00', NULL, NULL),
(39, 'xxx@xxx.xom', '9dc096e5ba9292ce87406d1be59c2358', 'xxx', 100, 1, NULL, '2017-01-01 00:00:00', NULL, NULL),
(40, 'telefonico-1@tienda.com', '6f9554abfb0b3daab56f933fc71abb42', 'Usuario Telefónico 1', 10, 1, NULL, '2017-01-01 00:00:00', NULL, NULL),
(45, 'asif@asif.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Asif Malik', 0, 1, NULL, '2020-05-22 19:12:56', NULL, NULL),
(46, NULL, NULL, NULL, 0, 1, NULL, '2020-05-25 22:46:23', NULL, NULL),
(47, NULL, NULL, NULL, 0, 1, NULL, '2020-05-26 13:17:17', NULL, NULL),
(48, NULL, NULL, NULL, 0, 1, NULL, '2020-05-26 18:47:03', NULL, NULL),
(49, 'alvaro@alvaro.com', '15d164d96de3a2fa66e776999f94ff27', 'Alvaro Moreno', 0, 1, NULL, '2020-05-27 21:25:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblzona`
--

CREATE TABLE `tblzona` (
  `idZona` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `intEstado` int(11) NOT NULL,
  `refPadre` int(11) NOT NULL,
  `dblInferior` double NOT NULL,
  `dblSuperior` double NOT NULL,
  `dblCoste` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblzona`
--

INSERT INTO `tblzona` (`idZona`, `strNombre`, `intEstado`, `refPadre`, `dblInferior`, `dblSuperior`, `dblCoste`) VALUES
(1, 'Europa', 1, 0, 0, 0, 0),
(2, 'EEUU', 1, 0, 0, 0, 0),
(3, 'Peso minimo', 1, 2, -1, 10, 30),
(4, 'Peso Medio', 1, 2, 10, 50, 100),
(5, 'Peso Grande', 1, 2, 50, 100, 120),
(6, 'Peso Gigante', 1, 2, 100, 999999, 125),
(7, 'Zona Única', 1, 1, -1, 999999, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblzonaimpuesto`
--

CREATE TABLE `tblzonaimpuesto` (
  `idZonaImpuesto` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `intEstado` int(11) NOT NULL,
  `dblCoste` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblzonaimpuesto`
--

INSERT INTO `tblzonaimpuesto` (`idZonaImpuesto`, `strNombre`, `intEstado`, `dblCoste`) VALUES
(8, 'Zona España y Europa', 1, 21),
(9, 'Resto del mundo', 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblcaracteristica`
--
ALTER TABLE `tblcaracteristica`
  ADD PRIMARY KEY (`idCaracteristica`);

--
-- Indices de la tabla `tblcarrito`
--
ALTER TABLE `tblcarrito`
  ADD PRIMARY KEY (`idContador`);

--
-- Indices de la tabla `tblcarritodetalle`
--
ALTER TABLE `tblcarritodetalle`
  ADD PRIMARY KEY (`idContadorDetalle`);

--
-- Indices de la tabla `tblcategoria`
--
ALTER TABLE `tblcategoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `tblcomentario`
--
ALTER TABLE `tblcomentario`
  ADD PRIMARY KEY (`idComentario`);

--
-- Indices de la tabla `tblcomparar`
--
ALTER TABLE `tblcomparar`
  ADD PRIMARY KEY (`idComparar`);

--
-- Indices de la tabla `tblcompra`
--
ALTER TABLE `tblcompra`
  ADD PRIMARY KEY (`idCompra`);

--
-- Indices de la tabla `tblconfiguracion`
--
ALTER TABLE `tblconfiguracion`
  ADD PRIMARY KEY (`idConfiguracion`);

--
-- Indices de la tabla `tbldeseo`
--
ALTER TABLE `tbldeseo`
  ADD PRIMARY KEY (`idDeseo`);

--
-- Indices de la tabla `tblidioma`
--
ALTER TABLE `tblidioma`
  ADD PRIMARY KEY (`idIdioma`);

--
-- Indices de la tabla `tblimpuesto`
--
ALTER TABLE `tblimpuesto`
  ADD PRIMARY KEY (`idImpuesto`);

--
-- Indices de la tabla `tblmarca`
--
ALTER TABLE `tblmarca`
  ADD PRIMARY KEY (`idMarca`);

--
-- Indices de la tabla `tblmoneda`
--
ALTER TABLE `tblmoneda`
  ADD PRIMARY KEY (`idMoneda`);

--
-- Indices de la tabla `tblopcion`
--
ALTER TABLE `tblopcion`
  ADD PRIMARY KEY (`idOpcion`);

--
-- Indices de la tabla `tblpreciogrupo`
--
ALTER TABLE `tblpreciogrupo`
  ADD PRIMARY KEY (`idGrupo`);

--
-- Indices de la tabla `tblproducto`
--
ALTER TABLE `tblproducto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `tblproductocaracteristica`
--
ALTER TABLE `tblproductocaracteristica`
  ADD PRIMARY KEY (`idProductocaracteristica`);

--
-- Indices de la tabla `tblproductoopcion`
--
ALTER TABLE `tblproductoopcion`
  ADD PRIMARY KEY (`idProductoOpcion`);

--
-- Indices de la tabla `tblproductovisita`
--
ALTER TABLE `tblproductovisita`
  ADD PRIMARY KEY (`idProductovisita`);

--
-- Indices de la tabla `tblslider`
--
ALTER TABLE `tblslider`
  ADD PRIMARY KEY (`idSlider`);

--
-- Indices de la tabla `tblusuario`
--
ALTER TABLE `tblusuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD UNIQUE KEY `strEmail` (`strEmail`);

--
-- Indices de la tabla `tblzona`
--
ALTER TABLE `tblzona`
  ADD PRIMARY KEY (`idZona`);

--
-- Indices de la tabla `tblzonaimpuesto`
--
ALTER TABLE `tblzonaimpuesto`
  ADD PRIMARY KEY (`idZonaImpuesto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblcaracteristica`
--
ALTER TABLE `tblcaracteristica`
  MODIFY `idCaracteristica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `tblcarrito`
--
ALTER TABLE `tblcarrito`
  MODIFY `idContador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT de la tabla `tblcarritodetalle`
--
ALTER TABLE `tblcarritodetalle`
  MODIFY `idContadorDetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `tblcategoria`
--
ALTER TABLE `tblcategoria`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tblcomentario`
--
ALTER TABLE `tblcomentario`
  MODIFY `idComentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `tblcomparar`
--
ALTER TABLE `tblcomparar`
  MODIFY `idComparar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `tblcompra`
--
ALTER TABLE `tblcompra`
  MODIFY `idCompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tblconfiguracion`
--
ALTER TABLE `tblconfiguracion`
  MODIFY `idConfiguracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbldeseo`
--
ALTER TABLE `tbldeseo`
  MODIFY `idDeseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `tblidioma`
--
ALTER TABLE `tblidioma`
  MODIFY `idIdioma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tblimpuesto`
--
ALTER TABLE `tblimpuesto`
  MODIFY `idImpuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tblmarca`
--
ALTER TABLE `tblmarca`
  MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tblmoneda`
--
ALTER TABLE `tblmoneda`
  MODIFY `idMoneda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tblopcion`
--
ALTER TABLE `tblopcion`
  MODIFY `idOpcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tblpreciogrupo`
--
ALTER TABLE `tblpreciogrupo`
  MODIFY `idGrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tblproducto`
--
ALTER TABLE `tblproducto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `tblproductocaracteristica`
--
ALTER TABLE `tblproductocaracteristica`
  MODIFY `idProductocaracteristica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `tblproductoopcion`
--
ALTER TABLE `tblproductoopcion`
  MODIFY `idProductoOpcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tblproductovisita`
--
ALTER TABLE `tblproductovisita`
  MODIFY `idProductovisita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT de la tabla `tblslider`
--
ALTER TABLE `tblslider`
  MODIFY `idSlider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tblusuario`
--
ALTER TABLE `tblusuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `tblzona`
--
ALTER TABLE `tblzona`
  MODIFY `idZona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tblzonaimpuesto`
--
ALTER TABLE `tblzonaimpuesto`
  MODIFY `idZonaImpuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
