<?php 

//isset($_SESSION['tienda2017Front_UserId']))

$query_DatosConsulta = sprintf("SELECT c.*,m.strSimbolo as simbolo FROM tblcompra c INNER JOIN tblmoneda m on c.refMoneda = m.idMoneda WHERE c.idUsuario=".$_SESSION['tienda2017Front_UserId']);
	
$DatosConsulta = mysqli_query($con,  $query_DatosConsulta) or die(mysqli_error($con));
$row_DatosConsulta = mysqli_fetch_assoc($DatosConsulta);
$totalRows_DatosConsulta = mysqli_num_rows($DatosConsulta);


?>




<?php if ($totalRows_DatosConsulta > 0) {  ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                         
                                            <th><?php echo _T123;?></th>
                                            <th><?php echo _T121;?></th>
                                            <th><?php echo _T48;?></th>
                                            <th><?php echo _T122;?></th>
                                            <th><?php echo _T65;?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
		//AQUI ES DONDE SE SACAN LOS DATOS, SE COMPRUEBA QUE HAY RESULTADOS
		
			 do { 
              		?>
              		
				<tr>
						<td><?php echo $row_DatosConsulta["idCompra"];?></td>
						
					<td>(Id: <?php echo $row_DatosConsulta["idUsuario"];?>) <?php echo ObtenerNombreUsuario( $row_DatosConsulta["idUsuario"])?></td>
						<td><?php echo DateToHumano($row_DatosConsulta["fchCompra"]);?> <?php echo HoraToHumano($row_DatosConsulta["fchCompra"]);?></td>
                    <td><?php echo ($row_DatosConsulta["dblTotalIVA"])." ".($row_DatosConsulta["simbolo"]);?></td>
						<td><?php echo MostrarEstadoPedido($row_DatosConsulta["intEstado"]);?>
                    </td>
						
					<td><a href="pedido-detalle-elegido.php?id=<?php echo $row_DatosConsulta["idCompra"];?>" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a></td>
				</tr>
                                        
              		
              		<?php
              		 } while ($row_DatosConsulta = mysqli_fetch_assoc($DatosConsulta)); 
	?>
   
                                    </tbody>
                                </table>
                          <?php      
        } 
		else
		 { //MOSTRAR SI NO HAY RESULTADOS ?>
                No hay resultados.
                <?php } ?>