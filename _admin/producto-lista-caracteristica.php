<?php require_once('../Connections/conexion.php');
RestringirAcceso("1, 10");?><?php


$variable_Consulta = "0";
if (isset($VARIABLE)) {
  $variable_Consulta = $VARIABLE;
}



	$query_DatosConsulta = sprintf("SELECT p.* FROM tblproducto p INNER JOIN tblcaracteristica c on c.refPadre = p.idProducto where c.refPadre=%s GROUP BY p.idProducto",                          GetSQLValueString($_GET["id"], "int") );
    
 //echo $query_DatosConsulta;


$DatosConsulta = mysqli_query($con,  $query_DatosConsulta) or die(mysqli_error($con));
$row_DatosConsulta = mysqli_fetch_assoc($DatosConsulta);
$totalRows_DatosConsulta = mysqli_num_rows($DatosConsulta);


?>
             

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/Administracion.dwt.php" codeOutsideHTMLIsLocked="false" -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Administración Tienda </title>
    <!-- InstanceEndEditable -->
    <!-- Bootstrap Core CSS -->
    <?php include("../includes/adm-cabecera.php"); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="ContenidoAdmin" -->
<div id="wrapper">
  <!-- Navigation -->
  <?php include("../includes/adm-menu.php"); ?>
  <div id="page-wrapper">
     <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Gestión de Productos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

           
            
<div class="row">
	<div class="col-lg-5">
		<a href="caracteristica-lista.php" class="btn btn-outline btn-primary">Volver</a>
	</div>
</div>

      </br>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Resultado
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                               <?php if ($totalRows_DatosConsulta > 0) {  ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id </th>
                                            <th></th>
                                            <th>Nombre </th>
                                            <th>Precio</th>
                                            <th>Estado </th>
                                            <th>Marca </th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
		//AQUI ES DONDE SE SACAN LOS DATOS, SE COMPRUEBA QUE HAY RESULTADOS
		
			 do { 
              		?>
              		
				<tr>
						<td><?php echo $row_DatosConsulta["idProducto"];?></td>
						<td>
						<?php if ($row_DatosConsulta["strImagen1"]!=""){?>
						<img src="../images/productos/<?php echo $row_DatosConsulta["strImagen1"];?>" width="30" height="30" alt=""/>
						<?php }
						else
						{?>
						<img src="../images/usuarios/sinfoto.jpg" width="30" height="30" alt=""/>
						<?php }?></td>
					<td><?php echo $row_DatosConsulta["strNombre_1"];?></td>
						<td><?php echo $row_DatosConsulta["dblPrecio"];?></td>
						<td><?php echo MostrarEstado($row_DatosConsulta["intEstado"]);?></td>
						<td><?php echo MostrarMarca($row_DatosConsulta["refMarca"]);?></td>
					<td><a title="Editar Producto" href="producto-edit.php?id=<?php echo $row_DatosConsulta["idProducto"];?>" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a> <a title="Editar Opciones" href="productoopcion-edit.php?id=<?php echo $row_DatosConsulta["idProducto"];?>" class="btn btn-warning btn-circle"><i class="fa fa-circle-o "></i></a>  <a title="Editar Características" href="productocaracteristica-edit.php?id=<?php echo $row_DatosConsulta["idProducto"];?>" class="btn btn-warning btn-circle"><i class="fa fa-road "></i></a> <a title="Estadísticas" href="producto-stats.php?id=<?php echo $row_DatosConsulta["idProducto"];?>" class="btn btn-success btn-circle"><i class="fa fa-bar-chart-o  "></i></a> <a title="Duplicar Producto" href="producto-duplicar.php?id=<?php echo $row_DatosConsulta["idProducto"];?>" class="btn btn-info btn-circle"><i class="fa fa-gears"></i></a></td>
				</tr>
              		
              		<?php
              		 } while ($row_DatosConsulta = mysqli_fetch_assoc($DatosConsulta)); 
	?>
   
                                    </tbody>
                                </table>
                               
                          <?php      
        } 
		else
		 { //MOSTRAR SI NO HAY RESULTADOS ?>
                No hay resultados.
                <?php } ?>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                
                <!-- /.col-lg-6 -->
            </div>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- InstanceEndEditable --><!-- /#wrapper -->

     <?php include("../includes/adm-pie.php"); ?>
   

</body>

<!-- InstanceEnd --></html>
<?php
//AÑADIR AL FINAL DE LA PÁGINA
mysqli_free_result($DatosConsulta);
?>
