<?php require_once('../Connections/conexion.php');
RestringirAcceso("1");?>
<?php


$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}


if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "forminsertar")) {

    $updateSQL = sprintf("UPDATE tblproducto set refGrupo=%s  WHERE idProducto=%s",
					   GetSQLValueString($_POST["refGrupo"], "int"),
					   GetSQLValueString($_POST["idProducto"], "int"));
    //echo $updateSQL;
$Result1 = mysqli_query($con, $updateSQL) or die(mysqli_error($con));   
    $insertGoTo = "producto-lista.php";
  header(sprintf("Location: %s", $insertGoTo));
}

$query_PrecioGrupo = sprintf(("SELECT * from tblpreciogrupo where refpadre=0" ));
$PrecioGrupo = mysqli_query($con,  $query_PrecioGrupo) or die(mysqli_error($con));
$row_PrecioGrupo = mysqli_fetch_assoc($PrecioGrupo);
$totalRows_PrecioGrupo = mysqli_num_rows($PrecioGrupo);


$query_PrecioGrupoProducto = sprintf(("SELECT refGrupo from tblproducto where idProducto=%s" ),
					   GetSQLValueString($_GET["id"], "int"));
$PrecioGrupoProducto = mysqli_query($con,  $query_PrecioGrupoProducto) or die(mysqli_error($con));
$row_PrecioGrupoProducto = mysqli_fetch_assoc($PrecioGrupoProducto);
$totalRows_PrecioGrupoProducto = mysqli_num_rows($PrecioGrupoProducto);
?>
             

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/Administracion.dwt.php" codeOutsideHTMLIsLocked="false" -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Administración Tienda </title>
    <!-- InstanceEndEditable -->
    <!-- Bootstrap Core CSS -->
    <?php include("../includes/adm-cabecera.php"); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>

<body>
<!-- InstanceBeginEditable name="ContenidoAdmin" -->
<script src="scriptupload.js"></script>
<script src="../js/scriptadmin.js"></script>
<script src="../js/tinymce/tinymce.min.js"></script>



<div id="wrapper">
  <!-- Navigation -->
  <?php include("../includes/adm-menu.php"); ?>
  <div id="page-wrapper">
     <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Gestión de Precios de Grupo Productos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <a href="producto-lista.php" class="btn btn-outline btn-info">Volver atrás</a><br>
<br>

            
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
 
                        <div class="panel-body">
                            <form action="producto-edit-grupo.php" method="post" id="forminsertar" name="forminsertar" role="form"><div class="row">
                                <div class="col-lg-6">
                                 <div class="form-group">
			<label>Precio Grupo</label>
			<select name="refGrupo" class="form-control" id="refGrupo">
				<option value="0">Sin descuento por Grupo</option>
				<?php do { ?>
				<option value="<?php echo $row_PrecioGrupo["idGrupo"]?>" <?php if ($row_PrecioGrupoProducto["refGrupo"]==$row_PrecioGrupo["idGrupo"]) echo "selected"; ?>><?php echo $row_PrecioGrupo["strNombre"]?></option>
				<?php
              		 } while ($row_PrecioGrupo = mysqli_fetch_assoc($PrecioGrupo)); 
	?>
			</select>
		</div>

                                        <button type="submit" class="btn btn-success">Actualizar</button>
                                    <input name="idProducto" type="hidden" id="idProducto" value="<?php echo $_GET["id"];?>">
                                      <input name="MM_insert" type="hidden" id="MM_insert" value="forminsertar">
                                       
                                    
                              </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
    
								</div>
                                <!-- /.col-lg-6 (nested) -->
                            </div></form>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                
                <!-- /.col-lg-6 -->
            </div>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- InstanceEndEditable --><!-- /#wrapper -->

     <?php include("../includes/adm-pie.php"); ?>
   

</body>

<!-- InstanceEnd --></html>